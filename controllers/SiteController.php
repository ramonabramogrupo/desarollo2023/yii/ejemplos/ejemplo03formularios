<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFormulario()
    {
        $model = new \app\models\Formulario();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // quiero mostrar resultados
                return $this->render('resultados', [
                    "model" => $model
                ]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
        ]);
    }

    public function actionFormulario1()
    {
        $model = new \app\models\Formulario1();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                
                return $this->render("resultado1",[
                    "model" => $model
                ]);
            }
        }

        return $this->render('formulario1', [
            'model' => $model,
        ]);
    }
}
