<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model"=>$model,
    "attributes"=>[
        "nombre",
        "edad",
        "mes",
        "categoria",
        "dia",
        "fechaAlta",
        "aficionestexto",
        "opcionestexto"
    ]
]);