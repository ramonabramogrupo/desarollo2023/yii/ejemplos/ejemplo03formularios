<?php

namespace app\models;
use yii\base\Model;

class Formulario extends Model{
    public ?int $dia=null;
    public ?string $mes=null;
    public ?string $poblacion=null;

    public function attributeLabels():array{
        return [
            "dia"=>"Dia del mes",
            "mes" => "Mes",
            "poblacion" => "Poblacion",
        ];
    }

    public function rules():array{
        // calculo el ultimo dia del mes
        $fecha=new \DateTime(); // creo un objeto de tipo fecha
        $fecha->modify('last day of this month'); // cambiando el objeto
        $dia=$fecha->format('d'); // formateamos la salida del objeto

        return [
            [['dia','mes','poblacion'],'required'],
            [['dia'],'number','min'=>1,'max'=>$dia],
        ];
    }

    public function getMeses():array{
        return [
            "enero" => "Enero",
            "febrero" => "Febrero",
            "marzo" => "Marzo",
            "abril" => "Abril",
            "mayo" => "Mayo",
            "junio" => "Junio",
            "julio" => "Julio",
            "agosto" => "Agosto",
            "septiembre" => "Septiembre",
            "octubre" => "Octubre",
            "noviembre" => "Noviembre",
            "diciembre" => "Diciembre"
        ];
    }

    public function getPoblaciones():array{
        return [
            "sa" => "Santander",
            "la" => "Laredo",
            "re" => "Reinosa",
            "po" => "Potes"
        ];
    }

}
