<?php

namespace app\models;
use yii\base\Model;

class Formulario1 extends Model{
    public ?string  $nombre=null;
    public ?int $edad=null;
    public ?int $mes=null;
    public ?int $categoria=null;
    public ?string $dia=null;
    public array $aficiones=[];
    public array $opciones=[];
    public ?string $fechaAlta=null;

    public function attributeLabels():array{
        return [
            "nombre"=>"Nombre completo",
            "edad" => "Edad",
            "mes" => "Mes Uso",
            "categoria" => "Categoria Herramienta",
            "dia" => "Dia Uso",
            "aficiones" => "Aficiones",
            "opciones" => "Opciones alquiler",
            "fechaAlta" => "Fecha Alta Sistema"
        ];
    }

    public function rules():array{
        return [
            [['nombre','edad','mes','categoria','dia','aficiones','opciones','fechaAlta'],'safe'],
            
        ];
    }

    public function getMeses():array{
        return [
            1 => "Enero",
            2 => "Febrero",
            3 => "Marzo",
            4 => "Abril",
            5 => "Mayo",
            6 => "Junio",
            7 => "Julio",
            8 => "Agosto",
            9 => "Septiembre",
            10 => "Octubre",
            11 => "Noviembre",
            12 => "Diciembre"
        ];
    }

    public function getPoblaciones():array{
        return [
            "sa" => "Santander",
            "la" => "Laredo",
            "re" => "Reinosa",
            "po" => "Potes"
        ];
    }

    public function getMostraraficiones(){
        return [
            "lectura","deporte","tv","informatica"
        ];
    }

    public function getMostraropciones(){
        return [
            "dia completo", "con carga", "limpieza incluida"
        ];
    }

    public function getCategorias(){
        return [
            "A","B","C"
        ];
    }

    public function getDias(){
        return [
            "Domingo","Lunes","Miercoles","Viernes"
        ];
    }

    public function getAficionestexto(){
        return join(",",$this->aficiones);
    }

    public function getOpcionestexto(){
        return join(",",$this->opciones);
    }

}
